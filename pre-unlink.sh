if [ -e ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/removeFSLWrapper fsl_reg swap_subjectwise swap_voxelwise tbss_1_preproc tbss_2_reg tbss_3_postreg tbss_3_postreg_new tbss_4_prestats tbss_deproject tbss_fill tbss_non_FA tbss_non_FA_new tbss_skeleton tbss_sym tbss_x
fi
